# geobatchpy #

geobatchpy is a small console application written in python. It can be used to query geographic information for addresses from Web Service APIs in batch mode.

Currently geobatchpy uses the package "pygeocoder" as a backend to query geographic information from the Google Geocoding API. For future releases, an integration of the package "geopy" is planned, which can then be used to select more APIs, including "OpenStreetMap Nominatim" and "Bing Maps API". For more information about the packages see the respective project sites:

* [pygeocoder](https://pypi.python.org/pypi/pygeocoder)
* [geopy](https://pypi.python.org/pypi/geopy)

## Usage ##

Originally geobatchpy is a python script. In order to use it, you would need to have a python installation on your system. But since not everybody has python installed and knows how to use it, a Windows executable ("geobatchpy.exe") has been compiled using [PyInstaller](http://www.pyinstaller.org/). This way it's easy to use the script even if you don't have python installed on your system. In fact, you don't have to install anything, just use the exe and you're all set.

**In order to use geobatchpy you have to download the complete repository, not just the exe-file.**

> Click on "Downloads" on the left or use git to clone the repository.

Prepare the input file (see "Input/Output") and double-click the exe. That's it. Enjoy!

## Input/Output ##

The io-folder holds the input file and the output file. You can prepare the input file using a spreadsheet software, e.g. Excel, and export it as a csv-file into the io-folder. The delimiter should be ";". The first line is ignored by the script and should contain the headings. The output file is encoded accordingly and can also be imported into a spreadsheet software for further work. If you're using geobatchpy as a python script, the filenames can be adjusted in "geobatch.py".

## Caching ##

The script caches already queried address strings in the file "cache/default.json". This is useful if you have many addresses and cannot query them all at once. The script can be stopped (focus the console and press [CRTL]+[c]) and be run later again without the need to start all over again. The script first searches the cache for an address string and only performs an API call to fetch the coordinates, if it can't find the address in the cache. If you have problems with the cache, simply replace the content of the file "default.json" with "{}" (an empty JSON), which empties the cache.

## Using from source (python) ##

If you're on Windows, the use of Anaconda python from [Continuum](https://www.continuum.io/) is recommended. It reduces the pain of getting python up and running on a Windows system. Anaconda python also comes with the neat package manager "conda", which is essentially a replacement for virtualenv and pip. Click [here](http://conda.pydata.org/docs/_downloads/conda-pip-virtualenv-translator.html) for more infos.

The use of a dedicated virtualenv for geobatchpy is recommended.

Use a package manager such as conda or pip to install missing packages.
If you're using conda, the command is:
```
conda install pygeocoder
```

If you're using pip, the command is:

```
pip install pygeocoder
```

In your console, navigate to the geobatchpy folder and run the script:
```
python geobatch.py
```

## Contribution ##

Feel free to contact me, if you have any suggestions for improvements.
#!/usr/bin/python
# -*- coding: utf-8 -*-

import re


def remove_quotes_from_data(data):
    new_data = []

    for row in data:
        new_row = []
        for item in row:
            new_item = item.replace("\"", "")
            new_row.append(new_item)
        new_data.append(new_row)

    return new_data


def generate_valid_string(string, strictness=0):

    # First, the string is transformed into a unicode representation:
    string = string.decode("UTF-8", "strict")

    string = string.replace(u'Ä', u'Ae')
    string = string.replace(u'Ü', u'Ue')
    string = string.replace(u'Ö', u'Oe')

    string = string.replace(u'ä', u'ae')
    string = string.replace(u'ü', u'ue')
    string = string.replace(u'ö', u'oe')

    string = string.replace(u'ß', u'ss')

    string = string.replace(u'"', u'')
    string = re.sub(r'\W+', ' ', string)

    # It is stripped from the sides: (whitespace removed)
    string = string.lstrip()
    string = string.rstrip()

    if strictness >= 1:
        # It is transformed to lowercase:
        string = string.lower()

        # Some critical characters are replaced:
        string = string.replace(u'/', u' ')
        string = string.replace(u'-', u'')

    if strictness >= 2:
        string = string.replace(u' ', u'_')

        # limit the length of the string:
        string = (string[:25]) if len(string) > 25 else string

    # Now it is transformed back to ASCII representation:
    string = string.encode("ASCII", "ignore")

    return string

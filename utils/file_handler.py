# !/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import json
from collections import OrderedDict


def get_data_from_csv(filename, skip_lines=0, max_lines=0):
    """ Function used to read a csv-file """

    # globally defined data-list:
    data = []

    with open(filename, 'rb') as csv_file:

        # get the reader object:
        input_csv_lines = csv.reader(
            csv_file,
            delimiter=';',
            quotechar='|')

        line_list = list(input_csv_lines)

        # by default, all lines should be read:
        if max_lines == 0:
            max_lines = len(line_list)
        else:
            max_lines = max_lines + skip_lines

        # build up a list with the lines:
        for line_index in range(skip_lines, max_lines):
            if len(line_list[line_index]) is not 0:
                data.append(line_list[line_index])

    return data


def get_data_from_json(path_to_file):
    """ Function used to read a json file. """

    data = {}

    with open(path_to_file, 'rb') as data_file:
        data = json.load(data_file, object_pairs_hook=OrderedDict)

    return data


def write_data_to_csv(data, path_to_file):
    """ Function used to save a two-dimensional list to a csv file. """

    with open(path_to_file, 'wb') as csv_file:
        output_file = csv.writer(
            csv_file,
            delimiter=';',
            quotechar='|')
        for entry in data:
            output_file.writerow(entry)

    return True


def write_data_to_json(data, path_to_file):
    """ Function used to save a dictionary to a json file. """

    with open(path_to_file, 'w') as json_file:
        json.dump(data, json_file)

    return True

#!/usr/bin/python
# -*- coding: utf-8 -*-

""" This module can be used to retrieve geographic data from the Google Geocoding API """

from pygeocoder import Geocoder
from pygeocoder import GeocoderError
import sys
import time
from . import file_handler
from . import data_utilities


def get_address_string(street, zipcode, city, country):
    """ Function used to get an address string from single attributes """

    address_string = data_utilities.generate_valid_string(street, strictness=1) + ", "
    address_string += data_utilities.generate_valid_string(zipcode, strictness=1) + " "
    address_string += data_utilities.generate_valid_string(city, strictness=1) + ", "
    address_string += data_utilities.generate_valid_string(country, strictness=1)

    return address_string


def get_coordinates_from_google(address_string, cache_file):
    """ Function used to retrieve geographic data from the Google Geocoding API """

    coordinates_cache = file_handler.get_data_from_json(cache_file)

    if address_string not in coordinates_cache:

        print '[INFO] ', address_string, ' not found in coordinates cache. Performing a call to the Google Geocoding API.'

        coordinates_list = [0, 0]

        try:
            my_geocoder = Geocoder()
            results = my_geocoder.geocode(address_string)
            coordinates_list = results.coordinates
            sys.stdout.write("Successfully performed a call to the Google Geocoding API for: " + address_string + " => " + str(coordinates_list) + "\n\n")
            sys.stdout.flush()
            time.sleep(1)
        except GeocoderError:
            sys.stdout.write("There occurred an error performing a call to the Google Geocoding API for: " + address_string + "\n\n")
            sys.stdout.flush()

        if coordinates_list[0] != 0:
            coordinates_cache[address_string] = coordinates_list
            file_handler.write_data_to_json(coordinates_cache, cache_file)

        return coordinates_list

    else:
        return coordinates_cache[address_string]

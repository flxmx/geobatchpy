# !/usr/bin/python
# -*- coding: utf-8 -*-

from utils import file_handler
from utils import geocode_getter


##### > SETTINGS < ############################################################

input_file = "io/input.csv"
output_file = "io/output.csv"

cache_file = "cache/default.json"
# The cache stores all geographic information already queried from Google.
# If you're batch converting numerous addresses this can help to save time.

###############################################################################


def main():

    raw_input("Welcome to geobatchpy. Press [Enter] to continue.")
    print ""

    input_data = file_handler.get_data_from_csv(input_file, skip_lines=1)

    results = [['street', 'zipcode', 'city', 'country', 'address-string', 'latitude', 'longitude']]

    for data_set in input_data:

        result = []

        street = data_set[0]
        zipcode = data_set[1]
        city = data_set[2]
        country = data_set[3]

        address_string = geocode_getter.get_address_string(street, zipcode, city, country)

        print "Getting coordinates for '" + address_string + "'..."

        coordinates = geocode_getter.get_coordinates_from_google(address_string, cache_file)
        latitude = coordinates[0]
        longitude = coordinates[1]

        result.append(street)
        result.append(zipcode)
        result.append(city)
        result.append(country)
        result.append(address_string)
        result.append(latitude)
        result.append(longitude)

        results.append(result)

        print ""

    file_handler.write_data_to_csv(results, output_file)

    raw_input("Done. Press [Enter] to exit.")


if __name__ == "__main__":
    main()
